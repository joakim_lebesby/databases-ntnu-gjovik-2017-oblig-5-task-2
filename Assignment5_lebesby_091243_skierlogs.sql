-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05. Nov, 2017 01:01 AM
-- Server-versjon: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skierlogs`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `club`
--

CREATE TABLE `club` (
  `clubId` varchar(60) NOT NULL,
  `name` text NOT NULL,
  `city` text NOT NULL,
  `county` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `log`
--

CREATE TABLE `log` (
  `entry` int(11) NOT NULL,
  `totaldistance` int(11) NOT NULL,
  `fallYear` year(4) NOT NULL,
  `userName` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `logentry`
--

CREATE TABLE `logentry` (
  `fallYear` year(4) NOT NULL,
  `userName` varchar(60) NOT NULL,
  `date` date NOT NULL,
  `area` text NOT NULL,
  `distance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `season`
--

CREATE TABLE `season` (
  `fallYear` year(4) NOT NULL,
  `clubId` varchar(60) DEFAULT NULL,
  `userName` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier`
--

CREATE TABLE `skier` (
  `userName` varchar(60) NOT NULL,
  `firstName` text NOT NULL,
  `lastName` text NOT NULL,
  `yearOfBirth` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`clubId`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`fallYear`),
  ADD KEY `userName` (`userName`);

--
-- Indexes for table `logentry`
--
ALTER TABLE `logentry`
  ADD KEY `fallYear` (`fallYear`,`userName`),
  ADD KEY `userName` (`userName`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`fallYear`),
  ADD KEY `clubId` (`clubId`),
  ADD KEY `userName` (`userName`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`userName`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `fallYear` FOREIGN KEY (`fallYear`) REFERENCES `season` (`fallYear`),
  ADD CONSTRAINT `log_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `season` (`userName`);

--
-- Begrensninger for tabell `logentry`
--
ALTER TABLE `logentry`
  ADD CONSTRAINT `logentry_ibfk_1` FOREIGN KEY (`fallYear`) REFERENCES `log` (`fallYear`),
  ADD CONSTRAINT `logentry_ibfk_2` FOREIGN KEY (`userName`) REFERENCES `log` (`userName`);

--
-- Begrensninger for tabell `season`
--
ALTER TABLE `season`
  ADD CONSTRAINT `clubId` FOREIGN KEY (`clubId`) REFERENCES `club` (`clubId`),
  ADD CONSTRAINT `userName` FOREIGN KEY (`userName`) REFERENCES `skier` (`userName`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
